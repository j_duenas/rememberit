Program Information:
Version: 1.5
Updates/Fixes (After Verstion 1.0):
V.1.1-Remade program's readme file (not this one, the one in the program itself)
V.1.2-Added  Clear All Strings
V.1.3-Added a feature that allows you to see who wrote the note.
V.1.4-Entered a secret code. If you enter it, you will get a secret message. If you want it (Don't know why you would) email me at toxicityJ@gmail.com and I'll send it to you
V.1.5-I put up a warning that is displayed before you clear strings

Features:
New Note- You can enter up to seven new notes
Old Note- You can recall up to seven old notes
Clear Notes- Clears your notes.
View All Notes- Lets you view all your notes one at a time. Hit enter to change between notes.
Readme- A small readme file with a program description and version information
End Program- End Program- Ends the program.

